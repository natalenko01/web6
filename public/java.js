function ObnCena() {
    let s = document.getElementsByName("coffeeVid");
    let select = s[0];
    let price = 0;
    let prices = PolCena();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.coffeeVids[priceIndex];
    }
    let radioDiv = document.getElementById("radios");
    if (select.value == "2") {
        radioDiv.style.display = "block";
    } else if (select.value == "4") {
        radioDiv.style.display = "block";
    } else {
        radioDiv.style.display = "none";
    }
    let radios = document.getElementsByName("coffeeParam");
    radios.forEach(function(radio) {
        if (radio.checked) {
            let optionPrice = prices.coffeeParam[radio.value];
            if (optionPrice !== undefined) {
                price *= optionPrice;
            }
        }
    });
    let checkDiv = document.getElementById("checkboxes");
    if (select.value == "3") {
        checkDiv.style.display = "block";
    } else if (select.value == "4") {
        checkDiv.style.display = "block";
    }  else {
        checkDiv.style.display = "none";
    }
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });
    let coffeeCena = document.getElementById("coffeeCena");
    let k = document.getElementById("input").value;
    if (k.match(/^[0-9]+$/) != null) {
        coffeeCena.innerHTML = price * k + " рублей";
    } else if (k == "0") {
        coffeeCena.innerHTML = 0 + " рублей";
    } else {
        coffeeCena.innerHTML = "Неверно введены данные";
        alert("Введите целое положительное число");
    }
}

function PolCena() {
    return {
        coffeeVids: [120, 130, 125, 110],
        coffeeParam: {
            option1: 1.5,
            option2: 1,
        },
        prodProperties: {
            coffeeDost: 130,
            coffeeDost2: 160,
        }
    };
}
window.addEventListener("DOMContentLoaded", function(event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let s = document.getElementsByName("coffeeVid");
    let select = s[0];
    select.addEventListener("change", function(event) {
        let target = event.target;
        console.log(target.value);
        ObnCena();
    });
    let radios = document.getElementsByName("coffeeParam");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
            ObnCena();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            ObnCena();
        });
    });
    let inputs = document.getElementById("input");
    input.addEventListener("change", function(event) {
        let r = event.target;
        console.log(r.value);
        ObnCena();
    });
    ObnCena();
});